﻿
const resultCards = $('#result-cards');
let filters = {
    "alias": "",
    "ageFrom": "",
    "ageTo": "",
    "location": "",
    "race": "",
    "releaseDate": "",
    "middleName": ""
};

$(function() {
    const submitButton = $('#submit-button');
    submitButton.on('click', function(e) {
        e.preventDefault();
        e.stopPropagation();
        const firstName = $('#first-name').val().trim();
        const lastName = $('#last-name').val().trim();
        const middleName = $('#middle-name').val().trim();
        const alias = $('#alias').val().trim();
        const releaseDate = $('#release-date').val().trim();
        const offenderId = $('#offender-id').val().trim();
        const race = $('#race').val().trim();
        const ageFrom = $('#age-from').val().trim();
        const ageTo = $('#age-to').val().trim();
        const sex = $('#sex').val().trim();
        let queryString = `/api/offenders?offenderId=${offenderId}&firstName=${firstName}&lastName=${lastName}` ;
        const formFields = [
            firstName,
            lastName,
            middleName,
            offenderId,
            alias,
            releaseDate,
            race,
            ageFrom,
            ageTo,
            sex
        ];
        queryString += firstName;

        filters["alias"] = alias;
        filters["race"] = race;
        filters["ageFrom"] = ageFrom;
        filters["ageTo"] = ageTo;
        filters["sex"] = sex;
        filters["middleName"] = middleName;
        filters["releaseDate"] = releaseDate;

        console.log(queryString)
        $.ajax({
          url: "https://api.myjson.com/bins/hyyt7",
          method: "get",
          context: document.body
        }).done(function (response) {
            for (var i = 0; i < response.length; i++) {
                let match = true;
                const offender = response[i];
                const fullName = offender["fullName"] || "";
                const date = offender["releaseDate"] || "";
                const gender = offender["gender"] || "";
                const age = offender["age"] || "";
                const race = offender["race"] || "";
                const id = offender["id"] || "";
                const alias = offender["alias"] || "";
                const location = offender["location"] || "";

                for (filter in filters) {
                    if (filters[filter] != "" && offender[filter] != filters[filter]) {
                        match = false;
                    }
                }
                var cardTemplate = `
                    <li>
                        <ul class="result-data">
                            <li class="result-subgroup col-7 mar-r-1 mar-l-1">
                                <dl>
                                    <dt>Personal Information</dt>
                                    <dd class="h2-style">${fullName}</dd>
                                    <dt class="alias">Alias:</dt>
                                    <dd class="alias"><p>${alias}</p></dd>
                                    <dt class="accessibility-hidden">Age/Race/Sex</dt>
                                    <dd><p>${age}/${race}/${gender}</p></dd>
                                </dl>
                            </li>
                            <li class="result-subgroup col-4">
                                <dl>
                                    <dt>Offender I.D.#</dt>
                                    <dd class="h2-style">${id}</dd>
                                </dl>
                            </li>
                            <li class="result-subgroup col-6 mar-r-1">
                                <dl>
                                    <dt>Location</dt>
                                    <dd><a class="multiline-link" href="#">${location}</a></dd>
                                </dl>
                            </li>
                            <li class="result-subgroup col-4 mar-r-1">
                                <dl>
                                    <dt>Release Date</dt>
                                    <dd class="h2-style">${date}</dd>
                                </dl>
                            </li>
                            <li class="result-subgroup col-3">
                                <dl>
                                    <dt class="accessibility-hidden">J-Pay Send funds to this offender</dt>
                                    <dd class="result-jpay">
                                        <div class="tooltip">
                                            <h2>JPay</h2>
                                            <p>Send funds to offender</p>
                                        </div>
                                        <a href="#"><img src="/images/icons/j-pay.svg" class="card-icon" alt="j-pay icon" /></a>
                                    </dd>
                                    <dt class="accessibility-hidden">Offender's Profile</dt>
                                    <dd class="result-offender-page"><a href="#"><img src="/images/icons/visit.svg" class="card-icon" alt="visit offender page icon" /></a></dd>
                                </dl>
                            </li>
                        </ul>
                    </li>
                `;
                match ? resultCards.append(cardTemplate) : false;
            }
            $.each($('input'), function (index, field) {
                $(field).val('');
            });
        });
    })
});
